import requests

url = "http://localhost:8080/api/v2/findings/"

payload = 'active=true&description=new_vuln&test=1&title=new_vuln&severity=3&numerical_severity=3'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Accept': 'application/json',
  'Authorization': 'Token afde4ccbc745d80d5c8c88aef20cfca36b8abb1e'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
