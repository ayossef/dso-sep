#!/bin/bash
echo "OK"

DEFECTDOJO_SCAN_MINIMUM_SEVERITY="Info"
DEFECTDOJO_SCAN_ACTIVE="true"
DEFECTDOJO_SCAN_VERIFIED="true"
DEFECTDOJO_ENGAGEMENTID=1
DEFECTDOJO_SCAN_FILE="gl-sast.json"
DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS="true"
DEFECTDOJO_SCAN_PUSH_TO_JIRA="false"
DEFECTDOJO_SCAN_ENVIRONMENT="Default"
DEFECTDOJO_SCAN_TEST_TYPE="SAST"
export TODAY=$("date")
echo $TODAY
DEFECTDOJO_URL="http://146.190.38.49:8080/api/v2"
DEFECTDOJO_TOKEN="0796d3d492f5a8eab03992cee956a39a7dcccdab"
curl --fail --location --request POST "${DEFECTDOJO_URL}/import-scan/" \
            --header "Authorization: Token ${DEFECTDOJO_TOKEN}" \
            --form "scan_date=\"${TODAY}\"" \
            --form "minimum_severity=\"${DEFECTDOJO_SCAN_MINIMUM_SEVERITY}\"" \
            --form "active=\"${DEFECTDOJO_SCAN_ACTIVE}\"" \
            --form "verified=\"${DEFECTDOJO_SCAN_VERIFIED}\"" \
            --form "scan_type=\"${DEFECTDOJO_SCAN_TYPE}\"" \
            --form "engagement=\"${DEFECTDOJO_ENGAGEMENTID}\"" \
            --form "file=@${DEFECTDOJO_SCAN_FILE}" \
            --form "close_old_findings=\"${DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS}\"" \
            --form "push_to_jira=\"${DEFECTDOJO_SCAN_PUSH_TO_JIRA}\"" \
            --form "test_type=\"${DEFECTDOJO_SCAN_TEST_TYPE}\"" \
            --form "environment=\"${DEFECTDOJO_SCAN_ENVIRONMENT}\""
