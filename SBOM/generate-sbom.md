# SBOM Generation for Node Project


## 1. Download the project - clone

To download the project we need to run the following command 

```bash
git clone https://gitlab.com/ayossef/secure-estore
```


## 2. Install Cycleonedx
If npm is not installed. We can install it using the command 

```bash
curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
```
we install cyclonedx using npm

```bash
npm i @cyclonedx/cyclonedx-npm
```

## 3. Execure Cyclonedx and generate the SBOM file

we run the cyclonedx tool using npx

```bash
npx @cyclonedx/cyclonedx-npm --output-file sbom.json
```
