# Installing DT

OWASP Dependancy Track is an open source projcet that can be installed using docker compose 

You need to download the docker compose file using the command:

```bash
curl -LO https://dependencytrack.org/docker-compose.yml
```

Then you can run the containers by bringing the stack up using the command:

```bash
docker-compose up -d
```